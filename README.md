# CPP FULL CICD TEMPLATE
![pipeline](https://gitlab.com/ForTwisted/cpp-full-cicd-template/badges/main/pipeline.svg)
![coverage](https://gitlab.com/ForTwisted/cpp-full-cicd-template/badges/main/coverage.svg)

This repository is a template for easily enable CI/CD inside your projects.
