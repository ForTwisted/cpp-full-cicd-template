FROM debian:bullseye

ENV DEBIAN_FRONTEND noninteractive

# Compilation
RUN apt-get update && apt-get install --no-install-recommends -y \
    git \
    make \
    cmake \
    python3 \
    python3-pip \
    gcovr \
    gcc \
    g++
