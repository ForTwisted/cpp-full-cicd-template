#include "myAwesomeLibrary.hpp"
#include "gtest/gtest.h"

TEST(simple_int_addition, Awesome) {
  auto calulator = AwesomeClass();
  EXPECT_EQ(calulator.simple_int_addition(1,4), 5);
}