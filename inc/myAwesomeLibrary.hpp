#pragma once

class AwesomeClass {
public:
  AwesomeClass();
  ~AwesomeClass();
  int simple_int_addition(int a, int b);
};